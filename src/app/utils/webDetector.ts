export function canUseWebP() {
  const elem = document.createElement("canvas");

  if (!!(elem.getContext && elem.getContext("2d"))) {
    // was able or not to get WebP representation
    globalWebpDetector =
      elem.toDataURL("image/webp").indexOf("data:image/webp") === 0;
    return globalWebpDetector;
  }

  // very old browser like IE 8, canvas not supported
  globalWebpDetector = false;
  return false;
}

export let globalWebpDetector = false;

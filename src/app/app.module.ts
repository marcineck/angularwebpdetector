import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { WebpPipePipe } from "./pipes/webp-pipe.pipe";
import { canUseWebP } from "./utils/webDetector";
declare const Modernizr: any;

canUseWebP();
@NgModule({
  declarations: [AppComponent, WebpPipePipe],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

import { Component, OnInit } from "@angular/core";
import { globalWebpDetector } from "./utils/webDetector";
declare const Modernizr: any;
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  title = "webpng";
  webpSupport;

  ngOnInit() {
    this.webpSupport = globalWebpDetector;
  }

  webpStatus() {
    console.log("From file: ", globalWebpDetector);
  }
}

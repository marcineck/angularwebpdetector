import { Pipe, PipeTransform } from "@angular/core";
import { globalWebpDetector } from "../utils/webDetector";
declare const Modernizr: any;

@Pipe({
  name: "webpPipe"
})
export class WebpPipePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (globalWebpDetector) {
      return `${value}.webp`;
    } else {
      return `${value}.jpg`;
    }
  }
}
